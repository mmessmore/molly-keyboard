# Molly Image Directory

You can add images here that correlate to words in the $MOLLY\_ROOT/lib/words
file.  These will be randomly chosen as possible values for a letter when
pressed.  I recommend pictures of parents and granparents as 'Momma', 'Daddy',
'Grandma', etc.  Family pets work, as do favorite foods, toys, etc.  

Note\: Be careful pulling pictures off the internet as they may fall under
copyright restriction.  I don't condone or recommend this at all.  It's
always safest to use your own content.  Second best is to use royalty-free
stock images.
