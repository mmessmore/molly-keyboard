# Molly resources

## gen_alpha_num
(re)generates images of letters, numbers, and dice for numbers.  Requires
the excellent tool ImageMagik.  Feel free to edit or replace.

## images
where molly looks for image files to display

## words
a library of words to say to represent letter key pushes, one word per line
