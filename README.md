# molly-keyboard

molly-keyboard is a relatively simple python script that uses pygtk, and 
festival to provide educational audio-video feedback to a kid banging on
a keyboard like kids do.

It's inspiration was my daughter banging on a kayboard and getting mad because
it wasn't connected to anything, so the screen didn't change.  I realized I 
could make something that was naturally fun to her and make the feedback a 
little educational in a way that wasn't forced.

The key to its success is to add words for your child.  Add words they like
mixed with similar words that they don't know yet.  For example, they may
like 'apple' but not yet know the word for 'banana.'  

This may not be of any use to anyone except me, but I thought I'd share.

## Prerequisites

### pygtk 2.x
There is a good chance you have this already.
* Debian/Ubuntu/etc
    + apt-get install python-gtk2
* Fedora, RHEL, etc
    + yum install pygtk2
* FreeBSD
    + cd /usr/ports/x11-toolkits/py-gtk2 && make install clean

### festival
* Debian/Ubuntu/etc.
    + apt-get install festival
* Fedora, RHEL, etc.
    + yum install festival
* FreeBSD
    + cd /usr/ports/audio/festival && make install clean

There may be other packages you want to install to get access to better voices, etc.	I'll leave festival configuration and usage to someone else.

### a child
I recommend making or adopting one.

### a sturdy keyboard
I've found an old Dell AT101W holds up well to the beating.

### pictures
You can use the lib/gen\_alpha\_num script to generate pictures of
letters and numbers, however no pictures for words are included.
There is an example lib/words file, however feel free to modify it to
your liking.  Just ensure for each word WORD that there is a corresponding
lib/images/WORD.png file.  There is a convenience script lib/downsize that
will resize images under lib/images to be at most 300px wide and tall.
It worked well with my little monitor.

## Usage
```
usage: molly.py [-h] [-d] [-v] [-o OUTFILE] [-N]

A program to teach letters, numbers, and words to children who like to bang on
keyboards.

optional arguments:
  -h, --help            show this help message and exit
  -d, --debug           turn on debug messages
  -v, --verbose         turn on verbose messages
  -o OUTFILE, --outfile OUTFILE
                        set verbose/debug output to an alternate file
  -N, --neuter-exit     aggressively disable exiting by doing terrible things
```
