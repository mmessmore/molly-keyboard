#!/usr/bin/python

import pygtk
pygtk.require('2.0')
import gtk
import os
import subprocess
import sys
import random
import signal
import argparse

digits = range(0, 10)
letters = { 'A' : 'ay', 'B' : 'bee', 'C' : 'see', 'D' : 'dee', 'E' : 'eee',
	'F' : 'eff', 'G' : 'gee', 'H' : 'H', 'I' : 'aye', 'J' : 'jay',
	'K' : 'kay', 'L' : 'el', 'M' : 'em', 'N' : 'en', 'O' : 'O', 'P' : 'pee',
	'Q' : 'queue', 'R' : 'R', 'S' : 'ess', 'T' : 'tee', 'U' : 'you',
	'V' : 'vee', 'W' : 'double you', 'X' : 'ex', 'Y' : 'why', 'Z' : 'zee' }
	
class Molly:
	def neuter_exit(self):
		self.disabled_codes = {}
		self.win_codes = {}

		if not self.opts.neuter:
			return False

		self.disabled_codes[37] = 'Control_L'
		self.disabled_codes[105] = 'Control_R'
		self.disabled_codes[64] = 'Alt_L'
		self.disabled_codes[108] = 'Alt_R'
		self.disabled_codes[65] = 'space'
		self.disabled_codes[135] = 'Menu'
		self.disabled_codes[23] = 'Tab'
		self.win_codes[115] = 'Super_L'
		self.win_codes[116] = 'Super_R'

		for (i, j) in self.disabled_codes.items():
			self.debug("disabling %d:%s" % (i,j))
			subprocess.call(["xmodmap", "-e", "keycode %d = " % (i)])

		# This seems reaclly backwards, but it works
		for (i, j) in self.win_codes.items():
			self.debug("disabling %d:%s" % (i,j))
			subprocess.call(["xmodmap", "-e", "keycode %d = %s" % (i,j)])
			
				
		return True

	def de_neuter_exit(self):
		for (i, j) in self.disabled_codes.items():
			self.debug("re-enabling %d:%s" % (i,j))
			subprocess.call(["xmodmap", "-e", "keycode %d = %s" % (i, j)])
		# This seems reaclly backwards, but it works
		for (i, j) in self.win_codes.items():
			self.debug("re-renabling %d:%s" % (i,j))
			subprocess.call(["xmodmap", "-e", "keycode %d = " % (i)])
		
	def debug(self, msg):
		if self.opts.debug:
			self.opts.outfile.write("%s\n" % msg)

	def verbose(self, msg):
		if self.opts.verbose:
			self.opts.outfile.write("%s\n" % msg)

	def say(self, text):
		if self.pid != 0:
			if os.waitpid(self.pid,os.WNOHANG) == (0, 0):
				return False
		self.pid = subprocess.Popen(["./libexec/fest.sh", text]).pid
		return True

	def load_pic(self, word):
		return gtk.gdk.pixbuf_new_from_file("lib/images/%s.png" % word)

	def load_words(self):
		self.words = {}
		self.images = {}
		wordfile = open('lib/words')
		for line in wordfile:
			char = line[0].upper()
			word = line.strip()
			self.verbose("loading %s" % (word))
			if char in self.words:
				self.words[char].append(word)
			else:
				self.words[char] = [word]
			self.images[word] = self.load_pic(word)
			self.images[char] = self.load_pic(char)
		for i in digits:
			self.images[i] = self.load_pic(i)
			dice = "dice-%s" % (i)
			self.images[dice] = self.load_pic(dice)

	def escape(self):
		if self.esc >= 10:
			self.say("Goodbye.")
			self.window.destroy()
		elif self.esc == 2:
			self.say("Hit escape 10 times to exit.")
		self.esc = self.esc + 1
		return True

	def set_left(self, image):
		self.image_left.set_from_pixbuf(image)
	
	def set_right(self, image):
		self.image_right.set_from_pixbuf(image)

	def key_press_event(self, widget, event, data=None):

		if event.keyval == 65307:
			return self.escape()

		# for numbers
		if (event.keyval - 48) in digits:
			i = (event.keyval - 48)
			self.set_left(self.images[i])
			dice = "dice-%s" % (i)
			self.set_right(self.images[dice])
			self.say("%s.  The number '%s'" % (i, i))
			self.verbose("Number:%s" % (i))

			return True

		# assume letters
		try:
			char = chr(event.keyval).upper()
		except ValueError:
			self.debug("I don't recognize %s" % (event.keyval))
			return False
		try:
			word = random.choice(self.words[char])
		except KeyError:
			self.debug("I got nothing for %c" % char)
			return False

		self.set_right(self.images[word])
		self.set_left(self.images[char])
		self.say ("The letter '%s',  %s starts with the '%s'." % \
			(letters[char], word, letters[char]))
		self.verbose("Key:%s, Word:%s" % (char, word))
		self.esc = 0
		return True

	def destroy(self, widget, data=None):
		self.de_neuter_exit()
		gtk.main_quit()

	def __init__(self, opts):
		self.opts = opts
		self.esc = 0
		self.pid = 0
		self.load_words()

		self.neuter_exit()

		self.window = gtk.Window(gtk.WINDOW_TOPLEVEL)
		self.window.set_title("Molly's Keyboard")
		self.window.connect("key_press_event", self.key_press_event)
		self.window.connect("destroy", self.destroy)
		self.window.set_border_width(10)
		color = gtk.gdk.color_parse('#FFFFFF')
		self.window.modify_bg(gtk.STATE_NORMAL, color)
		
		self.hbox = gtk.HBox(True)
		self.image_left = gtk.Image()
		self.image_right = gtk.Image()
		self.window.add(self.hbox)
		self.hbox.pack_start(self.image_right)
		self.hbox.pack_start(self.image_left)

		self.image_left.show()
		self.image_right.show()
		self.hbox.show()
		self.window.show()
		if not self.opts.debug:
			self.window.fullscreen()

	def main(self):
		gtk.main()

def getopts():

	description="""
A program to teach letters, numbers, and words to children who like to bang
on keyboards.
"""

	parser = argparse.ArgumentParser(description=description)
	parser.add_argument('-d', '--debug', dest='debug', action='store_true',
			help='turn on debug messages')
	parser.add_argument('-v', '--verbose', dest='verbose', 
			action='store_true', 
			help='turn on verbose messages')
	parser.add_argument('-o', '--outfile',
			type=argparse.FileType('w'),
			default=sys.stdout,
			help='set verbose/debug output to an alternate file') 
	parser.add_argument('-N', '--neuter-exit',
			dest='neuter', 
			action='store_true',
			help='aggressively disable exiting by doing terrible things')
	
	return parser.parse_args()

def nothing():
	pass


if __name__ == "__main__":
	signal.signal(signal.SIGTERM, nothing)
	opts = getopts()
	molly = Molly(opts)
	molly.main()
